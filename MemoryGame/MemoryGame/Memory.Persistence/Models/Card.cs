﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Memory.Persistence.Models
{
    public class Card
    {
        [BsonId]
        public int id { get; set; }
        public int cardTypeId { get; set; }
        public bool isFaceUp { get; set; }
        public bool isPermanentlyFlipped { get; set; }
    }
}
