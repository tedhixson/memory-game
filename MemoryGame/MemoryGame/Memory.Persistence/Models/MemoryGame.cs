﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Memory.Persistence.Models
{
    public class MemoryGame
    {
        [BsonId]
        public string id { get; set; }

        public int numberOfCards { get; set; }

        public int turnsTaken { get; set; }

        public bool isFinished { get; set; }

        public List<Card> cards { get; set; }
        public int numberOfMatches { get; set; }
        public int numCardsFlipped { get; set; }
        public int numCardsPermanentlyFlipped { get; set; }

        public Card lastCardFlipped { get; set; }
    }
}
