﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Memory.Persistence.Models
{

    public class MemoryGameDatabaseSettings : IMemoryGameDatabaseSettings
    {
        public string MemoryGamesCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IMemoryGameDatabaseSettings
    {
        string MemoryGamesCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
