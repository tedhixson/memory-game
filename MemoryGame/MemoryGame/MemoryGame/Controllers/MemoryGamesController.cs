﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemoryGame.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Memory.Persistence.Models;

namespace MemoryGame.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MemoryGamesController : ControllerBase
    {
        private readonly MemoryGamesService _service;
        private readonly ILogger _logger;

        public MemoryGamesController(MemoryGamesService service, ILogger logger)
        {
            _service = service;
            _logger = logger;

        }

        // GET api/memorygames
        [HttpGet]
        public  List<Memory.Persistence.Models.MemoryGame> Get()
        {
            var city = _service.Get();
            return city;
        }

        [HttpGet("{id}", Name = "GetMemoryGame")]
        public ActionResult<Memory.Persistence.Models.MemoryGame> Get(string id)
        {
            var game = _service.Get(id);

            if (game == null)
            {
                return NotFound();
            }

            return game;
        }

        [HttpPost]
        public ActionResult<Memory.Persistence.Models.MemoryGame> Create(Memory.Persistence.Models.MemoryGame game)
        {
            _service.Create(game);

            return CreatedAtRoute("GetMemoryGame", new { id = game.id }, game);
        }

        [HttpPut("{id}")]
        public IActionResult Update(string id, Memory.Persistence.Models.MemoryGame gameIn)
        {
            var game = _service.Get(id);

            if (game == null)
            {
                return NotFound();
            }

            _service.Update(id, gameIn);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var game = _service.Get(id);

            if (game == null)
            {
                return NotFound();
            }

            _service.Remove(game.id);

            return NoContent();
        }
    }
}