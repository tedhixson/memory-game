import { __decorate, __metadata } from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../../../app/app.constants';
import { catchError, tap } from 'rxjs/operators';
import { ErrorHandleService } from '../../shared/services/error-handle.service';
var LocationService = /** @class */ (function () {
    function LocationService(http, errorHandleService) {
        this.http = http;
        this.errorHandleService = errorHandleService;
    }
    LocationService.prototype.getDogBreedById = function (dogBreedId) {
        var uri = decodeURIComponent("https://api.thedogapi.com/v1/images/search?include_breed=1&breed_id=" + dogBreedId);
        return this.http.get(uri)
            .pipe(tap(function (_) { return console.log('fetched dog! Arf!'); }), catchError(this.errorHandleService.handleError('getDogBreedById', [])));
    };
    LocationService.prototype.postMemoryGame = function (memoryGame) {
        var uri = decodeURIComponent("" + Constants.gamesAPIUrl);
        return this.http.post(uri, memoryGame)
            .pipe(tap(function (_) { return console.log('posted memory game'); }), catchError(this.errorHandleService.handleError('postMemoryGame', null)));
    };
    LocationService.prototype.updateMemoryGame = function (id, memoryGame) {
        var uri = decodeURIComponent(Constants.gamesAPIUrl + "/" + id);
        return this.http.put(uri, memoryGame)
            .pipe(tap(function (_) { return console.log('updated memory game'); }), catchError(this.errorHandleService.handleError('updateMemoryGame', null)));
    };
    LocationService.prototype.getAllGames = function () {
        var uri = decodeURIComponent("" + Constants.gamesAPIUrl);
        return this.http.get(uri)
            .pipe(tap(function (_) { return console.log('updated memory game'); }), catchError(this.errorHandleService.handleError('updateMemoryGame', null)));
    };
    LocationService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [HttpClient,
            ErrorHandleService])
    ], LocationService);
    return LocationService;
}());
export { LocationService };
//# sourceMappingURL=location.service.js.map