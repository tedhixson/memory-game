import { __awaiter, __decorate, __generator, __metadata } from "tslib";
import { Component } from '@angular/core';
import { LocationService } from '../shared/services/location.service';
import { MemoryGame } from '../shared/models/memory-game';
import { Card } from '../shared/models/card';
import { DogBreed } from '../shared/models/dog-breed';
var WeatherComponent = /** @class */ (function () {
    function WeatherComponent(locationService) {
        this.locationService = locationService;
        // Cards
        this.numberOfCards = 8;
        this.doggos = [];
    }
    WeatherComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, unfinishedGame, newCards;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.errorMessage = null;
                        // Get all completed/active games
                        _a = this;
                        return [4 /*yield*/, this.GetAllMemoryGames()];
                    case 1:
                        // Get all completed/active games
                        _a.allGames = _b.sent();
                        unfinishedGame = this.GetLastIncompleteMemoryGame();
                        // If there's an unfinished game, keep playing
                        if (unfinishedGame) {
                            // Recreate new game from saved partial
                            this.memoryGame = new MemoryGame(unfinishedGame);
                            newCards = this.memoryGame.cards.map(function (a) { return new Card(a); });
                            this.memoryGame.cards = newCards;
                        }
                        // Otherwise create a new game
                        else {
                            this.InitializeAndSaveMemoryGame();
                        }
                        // Get dog data to populate cards
                        return [4 /*yield*/, this.fetchDogsArf()];
                    case 2:
                        // Get dog data to populate cards
                        _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WeatherComponent.prototype.InitializeMemoryGame = function () {
        this.memoryGame = new MemoryGame({ numberOfCards: this.numberOfCards });
        this.memoryGame.DealCards();
    };
    WeatherComponent.prototype.InitializeAndSaveMemoryGame = function () {
        this.InitializeMemoryGame();
        this.SaveCreatedMemoryGame(this.memoryGame);
    };
    WeatherComponent.prototype.GetLastIncompleteMemoryGame = function () {
        if (this.allGames && this.allGames.length > 0) {
            // Get all unfinished games
            var unfinishedGames = this.allGames.filter(function (a) { return a.isFinished == false; });
            // return the last one
            if (unfinishedGames.length > 0) {
                return unfinishedGames[unfinishedGames.length - 1];
            }
        }
        // Otherwise return null
        return null;
    };
    WeatherComponent.prototype.flipCard = function (card) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.memoryGame.FlipCard(card);
                        // After each move, update the game state
                        this.UpdateMemoryGame(this.memoryGame);
                        if (!this.memoryGame.isFinished) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.finishGame()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    WeatherComponent.prototype.startNewGame = function () {
        this.memoryGame.isFinished = true;
        this.finishGame();
    };
    WeatherComponent.prototype.finishGame = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        // Get all completed/active games
                        _a = this;
                        return [4 /*yield*/, this.GetAllMemoryGames()];
                    case 1:
                        // Get all completed/active games
                        _a.allGames = _b.sent();
                        this.InitializeAndSaveMemoryGame();
                        return [2 /*return*/];
                }
            });
        });
    };
    WeatherComponent.prototype.GetAllMemoryGames = function () {
        return __awaiter(this, void 0, void 0, function () {
            var retVal, promise;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        promise = new Promise(function (resolve, reject) {
                            _this.locationService.getAllGames()
                                .toPromise()
                                .then(function (res) {
                                retVal = res;
                                return resolve();
                            }, function (err) {
                                console.error(err);
                                _this.errorMessage = err;
                                reject(err);
                            });
                        });
                        return [4 /*yield*/, promise];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, retVal];
                }
            });
        });
    };
    WeatherComponent.prototype.SaveCreatedMemoryGame = function (game) {
        return __awaiter(this, void 0, void 0, function () {
            var promise;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        promise = new Promise(function (resolve, reject) {
                            _this.locationService.postMemoryGame(game).toPromise()
                                .then(function (res) {
                                return resolve();
                            }, function (err) {
                                console.error(err);
                                _this.errorMessage = err;
                                reject(err);
                            });
                        });
                        return [4 /*yield*/, promise];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WeatherComponent.prototype.UpdateMemoryGame = function (game) {
        return __awaiter(this, void 0, void 0, function () {
            var promise;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        promise = new Promise(function (resolve, reject) {
                            _this.locationService.updateMemoryGame(game.id, game).toPromise()
                                .then(function (res) {
                                return resolve();
                            }, function (err) {
                                console.error(err);
                                _this.errorMessage = err;
                                reject(err);
                            });
                        });
                        return [4 /*yield*/, promise];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // Dog stuff
    WeatherComponent.prototype.fetchDogsArf = function () {
        return __awaiter(this, void 0, void 0, function () {
            var numberOfMatches, doggos, i, doggo, breedInfo, imgUrl, dogBreed;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        numberOfMatches = this.numberOfCards / 2;
                        doggos = [];
                        i = 1;
                        _a.label = 1;
                    case 1:
                        if (!(i <= numberOfMatches)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.getDogBreedById(i)];
                    case 2:
                        doggo = _a.sent();
                        breedInfo = doggo[0].breeds[0];
                        imgUrl = doggo[0].url;
                        dogBreed = new DogBreed(breedInfo.id, breedInfo.name, imgUrl);
                        doggos.push(dogBreed);
                        _a.label = 3;
                    case 3:
                        i++;
                        return [3 /*break*/, 1];
                    case 4:
                        this.doggos = doggos;
                        return [2 /*return*/];
                }
            });
        });
    };
    WeatherComponent.prototype.getDogBreedById = function (breedId) {
        return __awaiter(this, void 0, void 0, function () {
            var retVal, promise;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        promise = new Promise(function (resolve, reject) {
                            _this.locationService.getDogBreedById(breedId)
                                .toPromise()
                                .then(function (res) {
                                retVal = res;
                                return resolve();
                            }, function (err) {
                                console.error(err);
                                _this.errorMessage = err;
                                reject(err);
                            });
                        });
                        return [4 /*yield*/, promise];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, retVal];
                }
            });
        });
    };
    WeatherComponent = __decorate([
        Component({
            selector: 'app-weather',
            templateUrl: './weather.component.html',
            styleUrls: ['./weather.component.css']
        }),
        __metadata("design:paramtypes", [LocationService])
    ], WeatherComponent);
    return WeatherComponent;
}());
export { WeatherComponent };
//# sourceMappingURL=weather.component.js.map