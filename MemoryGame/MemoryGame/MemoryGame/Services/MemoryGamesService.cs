﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Memory.Persistence.Models;

namespace MemoryGame.Services
{
    public class MemoryGamesService
    {
        private readonly IMongoCollection<Memory.Persistence.Models.MemoryGame> _games;

        public MemoryGamesService(IMemoryGameDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _games = database.GetCollection<Memory.Persistence.Models.MemoryGame>(settings.MemoryGamesCollectionName);
        }

        public List<Memory.Persistence.Models.MemoryGame> Get() =>
            _games.Find(game => true).ToList();

        public Memory.Persistence.Models.MemoryGame Get(string id) =>
            _games.Find<Memory.Persistence.Models.MemoryGame>(game => game.id == id).FirstOrDefault();

        public Memory.Persistence.Models.MemoryGame Create(Memory.Persistence.Models.MemoryGame game)
        {
            _games.InsertOne(game);
            return game;
        }

        public void Update(string id, Memory.Persistence.Models.MemoryGame gameIn) =>
            _games.ReplaceOne(game => game.id == id, gameIn);

        public void Remove(Memory.Persistence.Models.MemoryGame gameIn) =>
            _games.DeleteOne(game => game.id == gameIn.id);

        public void Remove(string id) =>
            _games.DeleteOne(game => game.id == id);
    }
}
