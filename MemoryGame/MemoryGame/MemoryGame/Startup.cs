﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemoryGame.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Serilog;
using Memory.Persistence.Config;
using Memory.Persistence.Models;

namespace MemoryGame
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            CurrentDirectoryHelpers.SetCurrentDirectory();
            // Configure Serilog
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)
                .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.Configure<MemoryGameDatabaseSettings>(
                Configuration.GetSection(nameof(MemoryGameDatabaseSettings)));

            services.AddSingleton<IMemoryGameDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<MemoryGameDatabaseSettings>>().Value);
            //Inject logger
            services.AddSingleton(Log.Logger);
            services.AddSingleton<MemoryGamesService>();
            services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "MemoryGameClient/dist";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseHttpsRedirection();

            app.UseCors("AllowAll");
            app.UseMvc();
            //app.UseSpa(spa =>
            //{
            //    spa.Options.SourcePath = "MemoryGameClient";
            //    if (env.IsDevelopment())
            //    {
            //        spa.Options.StartupTimeout = new TimeSpan(0, 0, 80);
            //        spa.UseAngularCliServer(npmScript: "start");
            //    }
            //});

        }
    }
}
