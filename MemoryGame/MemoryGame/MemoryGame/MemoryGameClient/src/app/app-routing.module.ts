import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MemoryGameComponent } from './memory-game/memory-game.component';

const routes: Routes = [
  { path: '', redirectTo: 'memory-game', pathMatch: 'full' },
  { path: 'memory-game', component: MemoryGameComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
