import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable, Subject, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, filter } from 'rxjs/operators';
import { MemoryGameCRUDService } from '../shared/services/memory-game-crud.service';
import { MemoryGame } from '../shared/models/memory-game';
import { Card } from '../shared/models/card';
import { DogBreed } from '../shared/models/dog-breed';
import { IMemoryGame } from '../shared/models/imemory-game';


@Component({
  selector: 'app-memory-game',
  templateUrl: './memory-game.component.html',
  styleUrls: ['./memory-game.component.css']
})
export class MemoryGameComponent implements OnInit {

  private errorMessage: string;

  // Cards
  private numberOfCards: number = 8;
  private memoryGame: MemoryGame;
  private allGames: IMemoryGame[];
  private doggos: DogBreed[] = [];

  constructor(
    private gameCrudService: MemoryGameCRUDService) {
  }

  async ngOnInit() {
    this.errorMessage = null;

    // Get all completed/active games
   this.allGames = await this.GetAllMemoryGames();

    let unfinishedGame = this.GetLastIncompleteMemoryGame();

    // If there's an unfinished game, keep playing
    if (unfinishedGame) {
      // Recreate new game from saved partial
      this.memoryGame = new MemoryGame(unfinishedGame);
      // Recreate each card from partial too
      let newCards = this.memoryGame.cards.map(a => new Card(a));
      this.memoryGame.cards = newCards;
    }
    // Otherwise create a new game
    else {
      this.InitializeAndSaveMemoryGame();
    }

    // Get dog data to populate cards
    await this.fetchDogsArf();
  }

  private InitializeMemoryGame() {
    this.memoryGame = new MemoryGame({ numberOfCards: this.numberOfCards });
    this.memoryGame.DealCards();
  }

  private InitializeAndSaveMemoryGame() {
    this.InitializeMemoryGame();
    this.SaveCreatedMemoryGame(this.memoryGame);
  }

  private GetLastIncompleteMemoryGame(): IMemoryGame {
    if (this.allGames && this.allGames.length > 0) {
      // Get all unfinished games
      let unfinishedGames = this.allGames.filter(a => a.isFinished == false);
      // return the last one
      if (unfinishedGames.length > 0) {
        return unfinishedGames[unfinishedGames.length - 1];
      }
    }
    // Otherwise return null
    return null;
  }

  private async flipCard(card: Card) {
    this.memoryGame.FlipCard(card);
    // After each move, update the game state
    this.UpdateMemoryGame(this.memoryGame);
    // If game is finished, start a new game!
    if (this.memoryGame.isFinished) {
      await this.finishGame();
    }
  }

  private startNewGame() {
    this.memoryGame.isFinished = true;
    this.finishGame();
  }

  private async finishGame() {
    // Get all completed/active games
    this.allGames = await this.GetAllMemoryGames();
    this.InitializeAndSaveMemoryGame();
  }

  async GetAllMemoryGames() {
    let retVal: IMemoryGame[];
    const promise = new Promise((resolve, reject) => {
      this.gameCrudService.getAllGames()
        .toPromise()
        .then(
          res => { // Success
            retVal = <IMemoryGame[]>res;
            return resolve();
          },
          err => {
            console.error(err);
            this.errorMessage = err;
            reject(err);
          }
        );
    });
    await promise;
    return retVal;
  }

  private async SaveCreatedMemoryGame(game: IMemoryGame) {
    const promise = new Promise((resolve, reject) => {
      this.gameCrudService.postMemoryGame(game).toPromise()
        .then(
          res => { // Success
            return resolve();
          },
          err => {
            console.error(err);
            this.errorMessage = err;
            reject(err);
          }
        );
    });
      await promise;
  }

  private async UpdateMemoryGame(game: IMemoryGame) {
    const promise = new Promise((resolve, reject) => {
      this.gameCrudService.updateMemoryGame(game.id, game).toPromise()
        .then(
          res => { // Success
            return resolve();
          },
          err => {
            console.error(err);
            this.errorMessage = err;
            reject(err);
          }
        );
    });
    await promise;
  }


  // Dog stuff

  private async fetchDogsArf() {
    let numberOfMatches = this.numberOfCards / 2;
    let doggos: DogBreed[] = [];
    for (let i = 1; i <= numberOfMatches; i++) {
      let doggo = await this.getDogBreedById(i);
      // Who's a good boy?
      let breedInfo = doggo[0].breeds[0];
      let imgUrl = doggo[0].url;
      let dogBreed = new DogBreed(breedInfo.id, breedInfo.name, imgUrl);
      doggos.push(dogBreed);
    }
    this.doggos = doggos;
  }

  async getDogBreedById(breedId: number) {
    let retVal;
    const promise = new Promise((resolve, reject) => {
      this.gameCrudService.getDogBreedById(breedId)
        .toPromise()
        .then(
          res => { // Success
            retVal = res;
            return resolve();
          },
          err => {
            console.error(err);
            this.errorMessage = err;
            reject(err);
          }
        );
    });
    await promise;
    return retVal;
  }

}
