import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Constants } from '../../../app/app.constants';
import { catchError, map, tap } from 'rxjs/operators';
import { ErrorHandleService } from '../../shared/services/error-handle.service';
import { IMemoryGame } from '../models/imemory-game';


@Injectable({
  providedIn: 'root'
})
export class MemoryGameCRUDService {

  constructor(
    private http: HttpClient,
    private errorHandleService: ErrorHandleService) { }


  postMemoryGame(memoryGame: IMemoryGame): Observable<IMemoryGame> {
    const uri = decodeURIComponent(`${Constants.gamesAPIUrl}`); 
    return this.http.post<IMemoryGame>(uri, memoryGame)
      .pipe(
        tap(_ => console.log('posted memory game')),
        catchError(this.errorHandleService.handleError('postMemoryGame', null))
      );
  }

  updateMemoryGame(id: string, memoryGame: IMemoryGame): Observable<IMemoryGame> {
    const uri = decodeURIComponent(`${Constants.gamesAPIUrl}/${id}`);
    return this.http.put<IMemoryGame>(uri, memoryGame)
      .pipe(
        tap(_ => console.log('updated memory game')),
        catchError(this.errorHandleService.handleError('updateMemoryGame', null))
      );
  }

  getAllGames(): Observable<IMemoryGame[]> {
    const uri = decodeURIComponent(`${Constants.gamesAPIUrl}`);
    return this.http.get<IMemoryGame>(uri)
      .pipe(
        tap(_ => console.log('updated memory game')),
        catchError(this.errorHandleService.handleError('updateMemoryGame', null))
      );
  }

  getDogBreedById(dogBreedId: number): Observable<any> {
    const uri = decodeURIComponent(
      `https://api.thedogapi.com/v1/images/search?include_breed=1&breed_id=${dogBreedId}`
    );
    return this.http.get<any[]>(uri)
      .pipe(
        tap(_ => console.log('fetched dog! Arf!')),
        catchError(this.errorHandleService.handleError('getDogBreedById', []))
      );
  }

}
