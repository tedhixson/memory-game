export class DogBreed {
  id: number;
  name: string;
  imgSource: string;

  public constructor(id: number, name: string, imgSource: string) {
    this.id = id;
    this.name = name;
    this.imgSource = imgSource;
  }
}
