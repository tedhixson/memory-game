import { ICard } from './icard';

export class Card implements ICard {
  public id: number;
  public cardTypeId: number;
  public isFaceUp: boolean = false;
  public isPermanentlyFlipped: boolean = false;


  public constructor(init?: Partial<ICard>) {
    Object.assign(this, init);
  }

  public Flip(setIsFaceUp?: boolean) {
    // We can either forcibly set the flipped state
    if (setIsFaceUp != null) {
      this.isFaceUp = setIsFaceUp;
    }
    // Or just blindly flip the current state
    else {
      this.isFaceUp = !this.isFaceUp;
    }
  }

  public SetCardPermanentlyFlipped() {
    this.isPermanentlyFlipped = this.isFaceUp = true;
  }

}
