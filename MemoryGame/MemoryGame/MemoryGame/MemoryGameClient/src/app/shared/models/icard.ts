export interface ICard {
  id: number;
  cardTypeId: number;
  isFaceUp: boolean;
  isPermanentlyFlipped: boolean;
}
