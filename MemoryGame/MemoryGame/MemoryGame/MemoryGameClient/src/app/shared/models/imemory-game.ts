import { Card } from './card';
import { ICard } from './icard';

export interface IMemoryGame {
  id: string;
  numberOfCards: number;
  turnsTaken: number;
  isFinished: boolean;
  cards: ICard[];

  numberOfMatches: number;
  numCardsFlipped: number;
  numCardsPermanentlyFlipped: number;
  // Keep track of last card flipped for matching purposes
  lastCardFlipped: ICard;
}
