import { Card } from './card';
import { Utility } from '../utility/utility';
import { IMemoryGame } from './imemory-game';
import { v4 as uuidv4 } from 'uuid';

const FLIP_DISPLAY_TIME = 1500;

export class MemoryGame implements IMemoryGame {
  public id: string;
  public numberOfCards: number;
  public turnsTaken: number = 0;
  public isFinished: boolean = false;
  public cards: Card[];

  public numberOfMatches = 0;
  public numCardsFlipped: number = 0;
  public numCardsPermanentlyFlipped: number = 0;
  // Keep track of last card flipped for matching purposes
  public lastCardFlipped: Card;


  public constructor(init?: Partial<IMemoryGame>) {
    Object.assign(this, init);

    if (!this.id) {
      // Generate new uuid as id
      this.id = uuidv4();
    }
    // We need 2 of each card
    this.numberOfMatches = this.numberOfCards / 2;
  }

  public DealCards() {
    // Start without any cards
    this.cards = [];
    this.lastCardFlipped = null;
    let currentCardId: number = 1;
    for (let i: number = 0; i < this.numberOfMatches; i++) {
      let cardType: number = i + 1;

      // Create two cards of the same type and add them to cards
      let firstCard: Card = new Card({ id: currentCardId, cardTypeId: cardType });
      this.cards.push(firstCard);
      currentCardId++;

      let matchingCard = new Card({ id: currentCardId, cardTypeId: cardType });
      this.cards.push(matchingCard);
      currentCardId++;
    }

    // After all cards have been created, shuffle them
    this.shuffleCards();
  }

  private shuffleCards() {
      var ctr = this.cards.length, temp, index;

      // While there are elements in the array
      while (ctr > 0) {
        // Pick a random index
        index = Utility.getRandomInt(ctr);
        // Decrease ctr by 1
        ctr--;
        // And swap the last element with it
        temp = this.cards[ctr];
        this.cards[ctr] = this.cards[index];
        this.cards[index] = temp;
      }
  }

  public FlipCard(card: Card) {
    // Permanently flipped cards don't count towards a turn
    if (!card.isPermanentlyFlipped) {
      card.Flip();
      this.numCardsFlipped++;

    // If we've flipped an even number of times, we've taken a turn
    if (this.numCardsFlipped % 2 == 0) {
      // Do cards match?
      // If so, then set them permanently flipped
      if (card.cardTypeId == this.lastCardFlipped.cardTypeId) {
        this.setCardPermanentlyFlipped(card);
        this.setCardPermanentlyFlipped(this.lastCardFlipped);
        // 
      }
      // Otherwise, make sure both are flipped face down after a short display time
      else {

        this.flipAfterDelay(card);
        this.flipAfterDelay(this.lastCardFlipped);
      }
      // Either way, the turn is over
      this.turnsTaken++;
      }

      // Is the game over?
      // This occurs when all the cards have been permanently flipped
      if (this.numCardsPermanentlyFlipped == this.numberOfCards) {
        this.FinishGame();
      }

      this.lastCardFlipped = card;

    }
  }

  private flipAfterDelay(card: Card) {
    setTimeout((cardInternal: Card) => {
      cardInternal.Flip(false);
    }, FLIP_DISPLAY_TIME, card)
  }

  private setCardPermanentlyFlipped(card: Card) {
    card.SetCardPermanentlyFlipped();
    this.numCardsPermanentlyFlipped++;
  }

  private FinishGame() {
    this.isFinished = true;
  }

}
